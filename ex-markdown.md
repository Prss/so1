# Exemplo marcação markdown

Extraído e adaptado de: 
[https://www.markdownguide.org/basic-syntax](https://www.markdownguide.org/basic-syntax)

## duas hash (##) = Cabeçalho nv 2

E testamos o texto novamente


###  três hash (###)= Cabeçalho nv 3

indo até 6 hash para um cabeçalho nv 6 (######)

## Formatação - negrito e itálico

Para dar ênfase usar asteristicos:

um texto com **ênfase** em uma palavra 
(2 asteristicos - negrito)

uma linha com *ênfase* em uma palavra
(1 esterisco - itálico)


## Citações 

Podemos fazer um bloco de citações de texto. Ele coloca em destaque um texto que estamos citando de outra fonte - texto que não foi escrito por nós - ou  que queremos destacar simplesmente. 

Assim:

> Isto agora é uma citação. Repare que o texto fica destacado do restante do documento .
>
>E a citação pode ter parágrafos também.
>
>> e também pode ter vários níveis 
>
> Assim fica uma citação dentro da outra.


## Listas 

Podemos criar listas usando - e .

## Uma lista não ordenada 

- Um item da lista
- Outro ítem
- mais um
- e assim vai...

## Uma lista ordenada

1. Esta lista tem uma ordem
1. cada ítem 
1. tem um 
1. número
1. indicando
1. a ordem que 
1. ele ocupa na lista 


## Imagens


![Tux, mascote do Linux](img/tux.avif)
![Ozob, JovemNerd](img/ozob.jfif)

## Blocos de Código

Este é um exemplo de código de python que foi copiado e colado aqui:

#!/usr/bin/python
experiencia = 20
pts_experiencia = 0.05
pts_magia = 0.5
robos = 2

resultado = (robos * pts_experiencia) + (robos * pts_magia) + (robos * experiencia)

print(f'Você ganhou {resultado} pontos!')


Bem ruím... Isso é porque a fonte usada para mostrar esse bloco de texto não é *monoespaçada* - os caracteres não ocupam todos o mesmo espaço na linha.

Precisamos usar uma fonte **monoespaçada* para representar corretamente um trecho de código.

``` python
#!/usr/bin/python
experiencia = 20
pts_experiencia = 0.5
pts_magia = 0.5
robos = 2

resultado = (robos * pts_experiencia) + (robos * pts_magia) + (robos * experiencia)

print(f'Você ganhou {resultado} pontos!')

```

## Links

Minha podcast preferido é o [Jovem Nerd](https://www.jovemnerd.com.br)

 - Link para outro documento (página)

Ex. Clique para ir para a [próxima página](pagina)